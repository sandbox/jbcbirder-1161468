function isArray(obj) {
   if (obj.constructor.toString().indexOf("Array") == -1)
      return false;
   else
      return true;
}
function allthisstuff(form) {

// get values from the Drupal Form
var dist = form.ebdistance.value;
var numdays = form.numdays.value;
var otherenid = form.getAttribute('nid');


// Run variables through validation functions - limit set to 20 days and 20 miles so as to not thrash the ebird servers.
if (lessThanX(dist) === false) {
  exit;
} else if (longerAgoThanX(numdays) === false) {
  exit;
} else if (IsNumeric(dist) === false) {
  exit;
} else if (IsNumeric(numdays) === false) {
  exit;
}

// make js variables out of saved Drupal settings 
var latitudes = Drupal.settings.Ebird_near.lat;
var longitudes = Drupal.settings.Ebird_near.longt;
var enid = Drupal.settings.Ebird_near.enid;

// test to see if variables are of type array (i.e. if there is more than one node on the page that was loaded)
// and then if it is an array, set lat, long, and enid to the element of the array that matches the node from which the button was clicked.
if (isArray(enid)) {
  for (x=0; x<=enid.length; x++) {
    if (enid[x] == otherenid) {
      var latitude = latitudes[x];
	  var longitude = longitudes[x];
	  tabledivenid = enid[x];
    }
  }
} else { // not an array just use the scalar value
  var latitude = latitudes;
  var longitude = longitudes;
  tabledivenid = enid;
}

path= 'ws1.1/data/obs/geo/recent?lng=' + longitude + '&lat=' + latitude + '&dist=' + dist + '&back=' + numdays + '&maxResults=500&locale=en_US&fmt=xml&includeProvisional=true';

$.ajax({ 
    type: "GET",

	url: proxypath + '/ebproxy2.php?eb_path=' + encodeURIComponent(path),
    
    dataType: "xml",
	success: parseXml

  });

} // END FUNCTION ALLTHISSTUFF  

function parseXml(xml)
{

  $("div[id=tablediv" + tabledivenid + "]").html("<table border='1' id='birdata'><tr><th>LOCATION</th><th>SPECIES</th><th>DATE/TIME</th></tr></table>");
  
  var i=0; // counter for striping
  //find every sighting and print the location, common name, and date/time
  $(xml).find("sighting").each(function() {
    var birdlocation = "<a href='http://maps.google.com/maps?q=" + $(this).find("lat").text() + "," + $(this).find("lng").text() + "'>" + $(this).find("loc-name").text() + "</a>";
    var birdspecies = $(this).find("com-name").text();
    var datetime = $(this).find("obs-dt").text();
	// Write the <tr>s
    if (i % 2 === 0) {
      $("table#birdata tr:last").after("<tr class='tr" + i + " stripe2'><td class='td1'>" + birdlocation + "</td><td class='td2'>" + birdspecies + "</td><td class='td3'>" + datetime + "</td></tr>");
    } else {
	  $("table#birdata tr:last").after("<tr class='tr" + i + " stripe1'><td class='td1'>" + birdlocation + "</td><td class='td2'>" + birdspecies + "</td><td class='td3'>" + datetime + "</td></tr>");
    }
	i++;
  });
}

// FORM VALIDATION //
function lessThanX (X) {
  if (X > 20) {
    alert("Distance must be 20 miles or less");
	return false;
  } else {
    return true;
  }
}
function longerAgoThanX (X) {
  if (X > 30) {
    alert("Number of Days back must be 30 days or less");
	return false;
  } else {
    return true;
  }
}
function IsNumeric(sText) {
   var ValidChars = "0123456789.";
   var IsNumber=true;
   var Char;
   for (i = 0; i < sText.length && IsNumber === true; i++) 
      { 
      Char = sText.charAt(i); 
      if (ValidChars.indexOf(Char) == -1) 
         {
		 alert("\"" + sText + "\"" + " is not a valid entry");
         IsNumber = false;
         }
      }
   if (!sText.length < 1 && sText > 0) {
     return IsNumber;
   } else {
     alert("Please fill in both values with a number greater than zero");
     return false;
   } 
}